#pragma once

#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>

#include "regmap/regmap.h"

#pragma GCC diagnostic ignored "-Wpedantic"
#include "felix/felix_fid.h"

typedef uint64_t elink_t;

struct to_flx_hdr
{
  uint32_t length;
  uint32_t reserved;
  elink_t elinkid;
};


void card_info(char* buffer);

