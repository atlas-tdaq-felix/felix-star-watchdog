#include <time.h>
#include <stdio.h>
#include <string.h>

#include "fifo.h"
#include "log.h"

static struct {
  int verbose;
  char* source;
  int error_fd;
} LOG_CONFIG;

void log_init(int verbose, char* source, char* error_fifo)
{
  LOG_CONFIG.verbose = verbose;
  LOG_CONFIG.source = source;
  LOG_CONFIG.error_fd = error_fifo == NULL ? 0 : open_fifo(error_fifo);
}

void _log(const char* level, const char* filename, unsigned line, const char *format, ...)
{
  if (!strcmp("DEBUG", level) && !LOG_CONFIG.verbose) return;

  va_list args;
  va_start(args, format);

  char timestamp[72];
  time_t ltime;
  ltime=time(NULL);
  struct tm *tm;
  tm=localtime(&ltime);

  sprintf(timestamp,"%04d-%02d-%02d %02d:%02d:%02d", tm->tm_year+1900, tm->tm_mon,
      tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);

  printf("%s [%s] ", timestamp, level);
  if(filename) {
    printf("(%s@%03d) ", filename, line);
  }
  char msg[1024];
  vsnprintf(msg, 1024, format, args);

  puts(msg);

  va_end(args);

  if(LOG_CONFIG.error_fd > 0) {
    // json Date Format: "2012-04-23T18:25:43.511Z"
    sprintf(timestamp,"%04d-%02d-%02dT%02d:%02d:%02d.000Z", tm->tm_year+1900, tm->tm_mon,
        tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec);
    error_write(LOG_CONFIG.error_fd, LOG_CONFIG.source, timestamp, level, msg);
  }
}

void log_flush()
{
  fflush(stdout);
}
