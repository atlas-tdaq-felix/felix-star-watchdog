#include <stdio.h>
#include <time.h>
#include "felix/felix_fid.h"
#include "netio/netio.h"

#define NUM_BUFFERS (256)
#define BUFFER_SIZE (4096)

netio_tag_t elinks[] = { 999 };
#define NUM_ELINKS (1)

struct {
  char* hostname;
  char* local_hostname;
  unsigned port;
  uint8_t vid;

  struct netio_context ctx;
  struct netio_subscribe_socket socket;
  struct netio_buffered_socket_attr socket_attr;
} config;

// Forward declarations
void on_init();
void on_connection_established(struct netio_buffered_recv_socket*);
void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t len);


void on_init()
{
  config.socket_attr.num_pages = NUM_BUFFERS;
  config.socket_attr.pagesize = BUFFER_SIZE;

  netio_subscribe_socket_init(&config.socket, &config.ctx, &config.socket_attr, config.local_hostname, config.hostname, config.port);
  config.socket.cb_msg_received = on_msg_received;

  uint32_t connector_offset = 0;
  uint8_t sid = 0;
  for(unsigned i=0; i<NUM_ELINKS; i++) {
    uint64_t fid = get_fid(connector_offset, elinks[i], sid, config.vid, 0, 0);
    printf("Subscribing to fid %lu \n",fid);
    netio_subscribe(&config.socket, fid);
    printf("Subscription returned \n");
  }
}


void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t len)
{
  printf("Tag=%lu, elink %u, msg size=%lu\n", tag, get_elink(tag), len);
  char *msg = (char*)data;
  for(unsigned int b=0; b<len; ++b){
   printf("%c", msg[b]);
  }
  printf("\n");
}


int main(int argc, char** argv)
{
  if(argc != 4) {
    fprintf(stderr, "%s: error: unexpected number of arguments\n", argv[0]);
    return 1;
  }

  config.local_hostname = argv[1];
  config.hostname = argv[2];
  config.port = atoi(argv[3]);
  config.vid = 1;

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;

  netio_run(&config.ctx.evloop);

  return 0;
}
