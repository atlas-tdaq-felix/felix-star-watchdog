#pragma once

#include <stdarg.h>

/* Example of json output

{
  "source": "127.0.0.1:50123",
  "timestamp": "2012-04-23T18:25:43.511Z",
  "level": "WARNING",
  "message": "The VMM is overheating, send icecream!"
}

date: https://stackoverflow.com/questions/10286204/the-right-json-date-format

level: CRITICAL, ERROR, WARNING, INFO, DEBUG, TRACE

*/

void log_init(int verbose, char* source, char* error_fifo);
void log_flush();

void _log(const char* level, const char* filename, unsigned line, const char *format, ...);

#define LOG_DBG(...)  _log("DEBUG",   NULL, 0, __VA_ARGS__)
#define LOG_INFO(...) _log("INFO",    NULL, 0, __VA_ARGS__)
#define LOG_WARN(...) _log("WARNING", NULL, 0, __VA_ARGS__)
#define LOG_ERR(...)  _log("ERROR",   NULL, 0, __VA_ARGS__)

#ifdef ENABLE_TRACE
# define LOG_TRACE(...) _log("TRACE", __FILE__, __LINE__, __VA_ARGS__)
#else
# define LOG_TRACE(...)
#endif
