#include <cstdio>
#include <cstring>
#include <limits.h>
#include <unistd.h>
#include <time.h>

#include "flxcard/FlxCard.h"

#include "nlohmann/json.hpp" 

extern "C" {
  #include "flx_data.h"
  #include "log.h"
}

//I will use card_info to get how many cards I have
//int N_devices = flxCard.number_of_devices();



using namespace nlohmann;

void card_info(char* buffer)
{
  json j;

  char hostname[HOST_NAME_MAX];
  gethostname(hostname, HOST_NAME_MAX);
  std::string host = std::string(hostname);
  j["Host"] = host;

  time_t timer;
  time(&timer);
  std::string tmstp = std::string(ctime(&timer));
  tmstp.erase(std::remove(tmstp.begin(), tmstp.end(), '\n'), tmstp.end());
  j["Time"] = tmstp;

  FlxCard* flxCard = new FlxCard();
  int n_cards = flxCard->number_of_devices()/2;

  for(int card_number=0; card_number<n_cards; ++card_number){

    std::string card_no = "card_"+std::to_string(card_number);

    //loop to get the device 0 of the desired card
    int dev0 = 0; int findzero =0;
    device_list_t devlist = flxCard->device_list();
    for (int loop = 0; loop < MAXCARDS; loop++){
      if (devlist.cdmap[loop][1] == 0){
        if (findzero == card_number){
          dev0 = loop;
          break;
        }      
        findzero++;
      }
    }
    flxCard->card_open(dev0, 0);
    //u_long baraddr2 = flxCard->openBackDoor(2);
  
    //FPGA STATUS
    //DNA
    u_long udna = flxCard->cfg_get_option(BF_FPGA_DNA);
    char cdna[20];
    sprintf(cdna, "0x%016lx", udna);
    std::string dna(cdna);
    j[card_no]["FPGA"]["DNA"] = dna;
  
    //temperature
    u_long ltemp = flxCard->cfg_get_option(BF_FPGA_CORE_TEMP);
    int ftemp = static_cast<int>(((float)ltemp * 503.975)/4096.0 - 273.15);
    j[card_no]["FPGA"]["temp"] = ftemp;

    //fan speed
    u_long lfan = flxCard->cfg_get_option(BF_TACH_CNT);
    int ffan = 600000000 / lfan;
    j[card_no]["FPGA"]["fan_speed"] = ffan;

    //clock
    u_long clk_sel = flxCard->cfg_get_option(BF_MMCM_MAIN_MAIN_INPUT);
    if(clk_sel==1){j[card_no]["FPGA"]["clock"] = "TTC";}
    if(clk_sel==2){j[card_no]["FPGA"]["clock"] = "LCLK";}
    else {j[card_no]["FPGA"]["clock"] = "???";}

    u_long clk_lock = flxCard->cfg_get_option(BF_MMCM_MAIN_PLL_LOCK);
    if(clk_lock & 1){j[card_no]["FPGA"]["CLK_lock"] = "YES";}
    else {j[card_no]["FPGA"]["CLK_lock"] = "NO";}

    //FIRMWARE
    //type
    u_long regval = flxCard->cfg_get_reg(REG_FIRMWARE_MODE);
    std::string fw_type;
    if     (regval == 0) fw_type = "GBT";
    else if(regval == 1) fw_type = "FULL";
    else if(regval == 2) fw_type = "LTDB";
    else if(regval == 3) fw_type = "FEI4";
    else if(regval == 4) fw_type = "PIXEL";
    else if(regval == 5) fw_type = "STRIP";
    else if(regval == 6) fw_type = "FELIG";
    else if(regval == 7) fw_type = "FMEMU";
    else if(regval == 8) fw_type = "MROD";
    else                 fw_type = "????";
    j[card_no]["FW"]["type"] = std::string(fw_type);
  
    //date
    u_int version_day = 0, version_month = 0, version_year = 0, version_hour = 0, version_minute = 0;
    u_long date = flxCard->cfg_get_option(BF_BOARD_ID_TIMESTAMP);
    version_year   = (date >> 32) & 0xFF;
    version_month  = (date >> 24) & 0xFF;
    version_day    = (date >> 16) & 0xFF;
    version_hour   = (date >>  8) & 0xFF;
    version_minute = (date >>  0) & 0xFF;
    char cdate[50];
    sprintf(cdate, "%02x/%02x/%02x %02x:%02x",version_year, version_month, version_day, version_hour, version_minute);
    j[card_no]["FW"]["date"] = std::string(cdate);
  
    //git hash
    char chash[20];
    u_long uhash = flxCard->cfg_get_option(BF_GIT_HASH);
    sprintf(chash,"0x%016lx", uhash);
    j[card_no]["FW"]["hash"] = std::string(chash);
  
    //reg map
    u_long reg_map_version = flxCard->cfg_get_option(BF_REG_MAP_VERSION);
    u_long major = (reg_map_version & 0xFF00) >> 8;
    u_long minor = (reg_map_version & 0x00FF) >> 0;
    char creg[20];
    sprintf(creg, "%lx.%lx", major, minor);
    j[card_no]["FW"]["reg_map"] = std::string(creg);
  
    //LINKS
    //alignment
    u_long aligned_data = flxCard->cfg_get_reg(REG_GBT_ALIGNMENT_DONE); 
    u_long number_channels = 2*flxCard->cfg_get_option(BF_NUM_OF_CHANNELS);
    u_long mask = 1, result = 0;
    for(u_long cont = 0; cont < number_channels; cont++){
      mask = (1ul << cont);
        result = aligned_data & mask;
        if(result >= 1)
          j[card_no]["alignment"][cont] = 1;
        else
          j[card_no]["alignment"][cont] = 0;
    }
    flxCard->card_close();
  }//end of loop on cards
  //serialise json
  std::string json_output = j.dump();
  //std::cout << json_output << std::endl;
  strcpy(buffer, json_output.c_str());
  delete flxCard;
}
