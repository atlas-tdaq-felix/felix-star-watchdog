// Program to publish diagnostic meassages over NetIO
// The e-link tag is configurable. The suggested is 999.

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "netio/netio.h"
#include "flx_data.h"
#include "log.h"

#define NUM_BUFFERS (256)
#define BUFFER_SIZE (4096)
#define WATERMARK (50)

struct {
  uint8_t vid;
  //network
  char* hostname;
  unsigned port;
  //netio
  struct netio_context ctx;
  struct netio_publish_socket socket;
  struct netio_timer timer;
  //for tag
  struct to_flx_hdr hdr;
  //data
  char* data;
  size_t datasize;
} config;


void on_timer(void* ptr)
{

  memset(config.data,0,1024);
  card_info(config.data);
  config.datasize = strlen(config.data);

  uint32_t connector_offset = 0;
  uint8_t sid = 0;
  uint64_t fid = get_fid(connector_offset, config.hdr.elinkid, sid, config.vid, 0, 0);

  int ret = netio_buffered_publish(&config.socket, fid, config.data, config.datasize, 0, NULL);
  printf("Published on fid %lu return %i  message size %lu %s \n", fid, ret, config.datasize, config.data);
  netio_buffered_publish_flush(&config.socket, fid, NULL);
}

static void on_subscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
   printf("New subscription for tag %lu\n", tag);
}

void on_init()
{
  struct netio_buffered_socket_attr attr;
  attr.num_pages = NUM_BUFFERS;
  attr.pagesize = BUFFER_SIZE;
  attr.watermark = WATERMARK;
  config.data = malloc(1024);
  netio_publish_socket_init(&config.socket, &config.ctx, config.hostname, config.port, &attr);
  config.socket.cb_subscribe = on_subscribe;
  netio_timer_start_ms(&config.timer, 1000);
}


int main(int argc, char** argv)
{
  if(argc != 4) {
    fprintf(stderr, "Usage: %s {IP addr} {port} {elink}\n", argv[0]);
    return 1;
  }

  config.vid = 1;
  config.hostname = argv[1];
  config.port = atoi(argv[2]);
  config.hdr.elinkid = atoi(argv[3]);
  printf("Publishing on interface %s : %i on elink %li \n", config.hostname, config.port, config.hdr.elinkid);
  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;

  netio_timer_init(&config.ctx.evloop, &config.timer);
  config.timer.cb = on_timer;

  netio_run(&config.ctx.evloop);

  return 0;
}
