## Description
Felix-star-watchdog is a utility that publishes diagnostic 
data regarding the Felix cards installed on the host.
Data is published over a selectable (virtual) e-link 
(then mapped onto a fid) in the form of a serialised 
json string.

Example with two cards:

> {"Host":"pc-tbed-felix-07.cern.ch","Time":"Sun May  3 15:06:29 2020","card_0":{"FPGA":{"CLK_lock":"YES","DNA":"0x012967c624508585","clock":"LCLK","fan_speed":8816,"temp":56},"FW":{"date":"19/10/23 11:54","hash":"0x00000000dc11cebb","reg_map":"4.8","type":"FULL"},"alignment":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]},"card_1":{"FPGA":{"CLK_lock":"YES","DNA":"0x012979e34d504285","clock":"LCLK","fan_speed":8750,"temp":-273},"FW":{"date":"19/10/23 11:54","hash":"0x00000000dc11cebb","reg_map":"4.8","type":"FULL"},"alignment":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}}


Example with one card

> {"Host":"pc-tbed-felix-04.cern.ch","Time":"Sun May  3 15:09:45 2020","card_0":{"FPGA":{"CLK_lock":"YES","DNA":"0x000800024c000185","clock":"LCLK","fan_speed":8683,"temp":64},"FW":{"date":"19/10/22 11:02","hash":"0x00000000dc11cebb","reg_map":"4.8","type":"GBT"},"alignment":[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]}}


## Usage
The server application is started using
```
./felix-watchdog <host ip> <port> <elink number>
```
The client is started with
```
./felix-watchdog-client <local ip> <Felix PC ip> <port>
```